import 'package:flutter/material.dart';

class GiftSection extends StatelessWidget {
  const GiftSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 16.0,
        left: 16.0,
        top: 32,
        bottom: 32,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GiftColumnWidget(
              numOfGift: '300', giftLabel: 'Received', color: Colors.blue),
          GiftColumnWidget(
              numOfGift: '98', giftLabel: 'Delivered', color: Colors.yellow),
          GiftColumnWidget(
            numOfGift: '1020',
            giftLabel: 'Friends',
            color: Colors.purple,
          )
        ],
      ),
    );
  }
}

class GiftColumnWidget extends StatelessWidget {
  final String numOfGift;
  final String giftLabel;
  final Color color;
  const GiftColumnWidget({
    Key key,
    this.numOfGift,
    this.giftLabel,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          //Number of gifts
          child: Text(numOfGift,
              style: TextStyle(color: this.color, fontSize: 26)),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 8.0, left: 8.0, bottom: 8.0),
          //Text for the corresponding number
          child: Text(giftLabel.toUpperCase(),
              style: TextStyle(color: Colors.grey)),
        ),
      ],
    );
  }
}
