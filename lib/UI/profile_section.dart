import 'package:flutter/material.dart';

class ProfileSection extends StatelessWidget {
  const ProfileSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //image
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage('images/profile_pic.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        //Name and Location
        Column(children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Bruce Miller',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Text(
            'San Fransisco, CA',
            style: TextStyle(fontSize: 17, color: Colors.grey),
          ),
        ]),
      ],
    );
  }
}
