import 'package:flutter/material.dart';

class BottomIconsSection extends StatelessWidget {
  const BottomIconsSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Icon(Icons.people, size: 30),
          Icon(Icons.business_center, size: 30),
          Icon(Icons.card_giftcard, size: 40, color: Colors.red),
          Icon(Icons.notifications, size: 30),
          Icon(Icons.person, size: 30),
        ],
      ),
    );
  }
}
