import 'package:flutter/material.dart';

class ActiveFriendsSection extends StatelessWidget {
  const ActiveFriendsSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: Column(
        children: [
          Text('3 most active friends including Ram, Shyam and Hari',
              softWrap: true,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 16,
              )),
          //friends profile section
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ProfileBuilder(imageName: 'ram.jpeg'),
                  ProfileBuilder(imageName: 'shyam.jpeg'),
                  ProfileBuilder(
                    imageName: 'hari.jpeg',
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class ProfileBuilder extends StatelessWidget {
  final String imageName;
  const ProfileBuilder({
    Key key,
    this.imageName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundImage: AssetImage('images/' + imageName),
    );
  }
}
