import 'package:flutter/material.dart';

class ViewFriendsSection extends StatelessWidget {
  const ViewFriendsSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: [
          Text(
            'View all friends',
            style: TextStyle(fontSize: 16, color: Colors.grey),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Transform.rotate(
              angle: 1.57058,
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.blue,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
