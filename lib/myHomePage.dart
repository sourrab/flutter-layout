import 'package:flutter/material.dart';

import 'UI/profile_section.dart';
import 'UI/gift_section.dart';
import 'UI/active_friends_section.dart';
import 'UI/view_friends_section.dart';
import 'UI/bottom_icons_section.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.menu),
        title: Text('Profile App'),
        actions: [
          Container(
            child: Icon(Icons.search),
            padding: EdgeInsets.only(right: 20),
          )
        ],
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 42),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    //profile-section
                    ProfileSection(),
                    //number-of-gift-Section
                    GiftSection(),
                    //active-friends-section
                    ActiveFriendsSection(),
                    //view-friends-section
                    ViewFriendsSection(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.blue[100],
      bottomSheet: BottomIconsSection(),
    );
  }
}
